# -*- coding: utf-8 -*-

import numpy as np 
import matplotlib.pyplot as plt
la = np.linalg

corpus = ['President Trump has many buildings .','Mr. Trump owns many factories in China .','President Trump has relatively small hands .']
words = ['buildings',
 'factories',
 'has',
 'owns',
 'President',
 'Trump',
 'many',
 'hands',
 'in',
 'Mr.',
 '.',
 'China',
 'small',
 'relatively']

X=np.array([[0 for i in range(14)] for j in range(14)])

for i in corpus:
	x=i.split()
	for j in xrange(len(x)-1):
		i1=words.index(x[j])
		i2=words.index(x[j+1])
		X[i1][i2]+=1
		X[i2][i1]+=1

U,s,Vh = la.svd(X, full_matrices = False)

fig, ax = plt.subplots()
fig=plt.scatter(U[:,0], U[:,1], alpha=0.5)

for i in xrange(len(words)):
	ax.text(U[i,0],U[i,1],words[i])

plt.autoscale()
plt.show()


